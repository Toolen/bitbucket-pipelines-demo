# Bitbucket Pipelines Demo
> Демонстрационный проект для доклада "Bitbucket CI/CD"

[Слайды](https://toolen.github.io/bitbucket-ci-cd/)

## Зависимости
*  Python 3.8
*  [Poetry](https://python-poetry.org/docs/#installation)
*  [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Подготовка сервера
1.  Арендовать сервер с внешним ip в любом облачном сервисе
2.  Прописать ip сервера в deployments
3.  Сгенерировать ssh ключ для пайплайна
4.  Добавить ip сервера в know hosts пайплайна
5.  Создать файл provisioning/inventory/hosts с ip адресом сервера
6.  Создать файл provisioning/roles/deployer/files/authorized_keys и скопировать туда публичный ssh ключ пайплайна
7.  Собрать билд командой:

```
poetry build  
```

8.  Перейти в папку provisioning и запустить плейбук. Например, если вы используете rsa ключ для доступа к серверу, то команда будет выглядеть так: 

```
ansible-playbook -i inventory/hosts --private-key <path to private key> -u <username> playbook.yml
```

9.  Открыть браузер по адресу <ip сервера>:8080. Если все прошло успешно, то вы увидите надпись: "Hello 1 times!"

## Test playbook.yml
Используем [Vagrant](https://www.vagrantup.com/) для проверки проверки плейбука. Кроме основных зависимостей, нужно установить:  

1.  [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
2.  [Vagrant](https://www.vagrantup.com/docs/installation)

Затем выполнить команды:
  
```
poetry build  
vagrant up  
```

Откройте браузер по адресу: [http://localhost:8080](http://localhost:8080). Если все прошло успешно, то вы увидите надпись: "Hello 1 times!"
