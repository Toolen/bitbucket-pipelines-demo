from aiohttp import web

from .constants import REDIS_POOL, REDIS_INC_KEY


async def index_handler(request):
    app = request.app
    redis = app[REDIS_POOL]
    value = await redis.incr(REDIS_INC_KEY)
    return web.Response(text=f'Hello {value} times!')


def setup_handlers(app):
    app.add_routes([
        web.get('/', index_handler, name='index'),
    ])
