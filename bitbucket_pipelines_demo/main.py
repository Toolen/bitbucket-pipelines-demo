from aiohttp import web

from .handlers import setup_handlers
from .redis import setup_redis


async def init_app():
    app = web.Application()
    await setup_redis(app)
    setup_handlers(app)
    return app


def main():
    app = init_app()
    web.run_app(app)


if __name__ == '__main__':
    main()
