import aioredis

from .constants import REDIS_POOL
from .settings import REDIS_URL


async def setup_redis(app):
    app[REDIS_POOL] = await aioredis.create_redis_pool(REDIS_URL)
    app.on_cleanup.append(close_redis)


async def close_redis(app):
    pool = app[REDIS_POOL]
    pool.close()
    await pool.wait_closed()
