import os

env = os.environ

REDIS_URL = env.get(
    'REDIS_URL',
    'redis://localhost:6379'
)
