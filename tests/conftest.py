import pytest

from bitbucket_pipelines_demo.main import init_app


@pytest.fixture
async def client(aiohttp_client):
    app = await init_app()
    return await aiohttp_client(app)
