from bitbucket_pipelines_demo import __version__


def test_version():
    assert __version__ == '0.1.0'


async def test_index(client):
    resp = await client.get('/')
    assert resp.status == 200
    text = await resp.text()
    assert isinstance(text, str)
